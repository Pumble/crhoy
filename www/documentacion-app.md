# DOCUMENTACION PARA EL APP CRHoy Lite
# POR: Juan Granados

# INDICE
# ==> PLATAFORMAS DE DESARROLLO
# ==> FRAMEWORKS
# ==> ESTRUCTURA DE PROYECTO
# ==> FUNCIONAMIENTO DEL PROYECTO
# ==> COMO PUBLICAR EL APP
# ==> PENDIENTES

# =====> PLATAFORMAS DE DESARROLLO
El proyecto se inicio desde 0 en el IDE online de Monaca(https://monaca.io/), esta herramienta provee un completo
entorno virtual para el desarrollo de aplicaciones en hibridas basadas en Cordova(https://cordova.apache.org/).
Una aplicacion Cordova esta basada en programacion web, por lo que se utilizan archivos HTML, CSS y JS para
hacerla funcionar. Estas aplicaciones corren en un browser interno de IOS o Android. En el caso de Android,
el motor es el mismo que se utiliza para Google Chrome, aunque no siempre contiene las ultimas compatibilidades
y "features" que van sacando. El de IOS esta basado en Safari y es muy posible encontrar errores de compatibilidad.

Una ventaja del IDE de Monaca es que nos permite instalar en los telefonos "debug" un app que nos permite
"debuggear" en caliente el app. Cada cambio hecho en el IDE es inmediatamente enviado al app para poder probar
si se ve y funciona como deseamos. El app esta disponible en Android(https://play.google.com/store/apps/details?id=mobi.monaca.debugger)
e IOS. Aqui pueden encontrar todo lo relaciona con el Monaca Debugger(https://docs.monaca.io/en/products_guide/debugger/)

El debugger funciona con la cuenta asociada, en nuestro caso utiliza mi correo de trabajo: juan.granados@crhoy.com y la contraseña
es RMat710! . El plan actual de Monaca es de pago, pero esta con mi tarjeta, asi que lo cancelare y lo dejare en el gratuito. No
deberia hacer falta que paguen más a menos de que necesiten varias builds diarias.

# =====> FRAMEWORKS
El app esta desarrollada bajo el framework Onsen(https://onsen.io/), en mi experiencia es de los mejores frameworks 
para aplicaciones Cordova, es muy rapido, eficiente y tiene todas las herramientas para crear un app rapidamente. 
La razon de utilizar un framework ESPECIAL par alicaciones Cordova es la compatibilidad. El equipo de Onsen(es facil
encontrarlos en Github o Stack overflow respondiendo dudas) dedica mucho tiempo a compatibilizar controles en IOS y 
Android(en todas sus versiones), existe una gama muy amplia de dispositivos con sus propias cualidades web, por lo 
que resulta ideal ahorrarse la frustacion de compatibilizar todos los errores.

Onsen tiene una completa guia(https://onsen.io/v2/guide/) para aprender a utilizar sus componentes, ademas tiene un 
API con toda la documentacion para sus elementos CSS(https://onsen.io/v2/api/css.html), asi como para utilizar
el Framework en combinacion con Vue, React, Angular o Vanilla JS.

En nuestro caso he decidido utilizar VueJS(https://vuejs.org/), las principales razones de esto son:
1. Es muy sencillo
2. Es mucho mas rapido que Angular1
3. La logica de servicios, componentes y directivas esta mucho mejor separada
4. No es pesado, es muy ligero comparado a Angular1
5. El render es increiblemente rapido, casi no se nota
Lo comparo con Angular 1 porque es el que utilizamos en el resto del sitio. Comparados entre ellos, en mi experiencia
VueJS lo supera por mucho, siendo este mucho mas entendible, accesible y amigable con el desarrollador. Aca les dejo
una completa comparacion de ambos frameworks(https://vuejs.org/v2/guide/comparison.html#AngularJS-Angular-1)

VueJS tiene una completa guia(https://vuejs.org/v2/guide/) la cual recomiendo leer para entender en profundidad el 
funcionamiento del mismo, asi como el del APP.

Es importante entender la importancia del framework dedicado para Cordova, las aplicaciones Cordova si bien
comparten mucho con cualquier aplicacion web tienen la principal limitante del web view del sistema operativo.
NO ES LO MISMO UTILIZAR ONSEN QUE BOOTSTRAP EN UNA APLICACION CORDOVA. Bootstrap no esta hecho para trabajar
en webviews ya que depende mucho hacks solo posibles en web. En Cordova podrian funcionar pero no estar 
compatibilizadas con todos los web views de Android o IOS.

# =====> ESTRUCTURA DE PROYECTO
El proyecto, como mencionamos antes esta pensado para aplicaciones Cordova, por lo que ya posee cierta estructura
a respetar. En Cordova existen 3 carpetas y un archivo muy importantes que se deben mantener para poder compilar
el app. Estas son:

==> plugins: en ella se guardan los plugins de cordova que utilicemos. Ul plugin de Cordova lo que hace es 
enlazar codigo nativo de Android o IOS a codigo de JS que podamos utilizar. Asi bien, para utilizar la 
camara del telefono es necesario instalar el plugin de camara, el cual a traves de un API conecta la camara a JS
para poder utilizarla asi: window.plugins.camera.takePhoto() <== esto es un ejemplo, pero es muy similar. En el
caso del app de CRHoy cuenta con los siguiente plugins: cordova-plugin-x-socialsharing, splashscreen, whitelist,
cordova-plugin-firebase.

==> res: Es la carpeta de recursos, aqui se pueden guardan los splashscreen e iconos del app para su publicacion.

==> www: Esta carpeta contiene todo lo que se va a utilizar en el app, los JS, CSS y HTML necesarios para el
funcionamiento, asi como las imagenes, librerias y demas extras necesarios para el app.

===> config.xml: Este archivo es el encargado de configurar todo para la compilacion del app, se puede utilizar
para establecer los splashscreen, configurar los plugins, los whitelist, tambien se configuran archivos que se
generan al compilar el app, como los info.plist de IOS.

Esto, seria a nivel de Cordova, ahora bien, dentro de la carpeta www hay toda una estructura de archivos para
la programacion del proyecto. Esta se detalla a continuacion:

> components
	> monaca-cordova-loader
	> monaca-core-itils
	> loader.css
	> loader.js
> css
	> components
	> pages
	> colors.css
	> core.css
	> fonts.css
> img
> js
	> components
	> config
	> directives
	> filters
	> services
> libs
	> onsenUI
		> css
		> js
	> vue-onsenui.min.js
	> vue-router.min.js
	> vue.min.js
	> vuejs-datepicker.min.js
> index.html

En los components(la primera) se encuentran archivos para cargar cordova.js en el app, no es importante, pero es mejor
dejarla tal como esta para evitar problemas al compilar.

En la carpeta de css se encuentra dividida en componentes y paginas, esto para separar e identificar donde aplica ese css.
core.css aplica para todo el app, colors.css estan los colores y en fonts.css todo lo relacionado con las fuentes.

En img estan las imagenes que se utilizan en el app.

En la carpeta js esta la parte importante del app, su logica. En esta parte es importante conocer bien el funcionamiento
de VueJS para entender el porque.
> components: primero esta el app.component.js que esta sobre todos los componentes, este corre en todo el app, los 
demas componentes corresponden a cada pagina del app. Cada componente require de un template, que le indica cual es 
su ambito de html. Los mixins son los servicios que se utilizaran en ese componente, no son globales para separar
mejor la logica y encapsular mejor el funcionamiento de la pagina. El apartado de data, mounted, methods y computed
estan bien explicados en VueJS.

> config: En config se guardan los archivos estaticos que sirven para configurar el app, las categorias por ejemplo,
las rutas, el apartado de redes sociales, etc. Tambien tiene un apartado de firebase, en el cual esta configurado
la cuenta y servicios de almacenamiento para problemas y notificaciones. Este servicio se encarga de registrar
los errores en una coleccion de datos del servicio firestore de 
Firebase(https://console.firebase.google.com/u/0/project/crhoy-lite/database/firestore/data~2Ferrors~2FXhlME6LYU3SHod7WQx8W)

> directives: Las directivas tienen componentes visuales con su propia logica, como acordiones, tab y el elemento de 
titulo(que es muy complejo). Las directivas funcionan similar a las de Angular 1, requieren de propiedades para
enviar datos al elemento html, tiene sus propios datos, metodos y eventos. A su vez, se debe declarar el template
que se utiliza de modelo para mostrar. Las directivas si son globales y se pueden utilizar en cualquiera de las
paginas.

> filters: Los filtros lo que hacen es transformar un dato a traves de filtros. Hay uno de HTML para evitar
caracteres raros y uno de icdn para obtener imagenes filtradas por el mismo. Son globales.

> services: Los servicios son los encargados de obtener los datos del app. Los metodos utilizan el API de 
VueResource(https://github.com/pagekit/vue-resource) para hacer llamadas HTTP Request. Basicamente lo hay
que hacer es configurar el servicio. Luego se incluye en los componentes en la parte de mixins y se
utiliza como un metodo dentro del elemento this. El servicio devuelve una promesa que ya se utiliza dentro
del metodo para poderlo ajustar a disposicion en cada componente.

> index.html: Este es el archivo inicial de proyecto, en este caso es sumamente importante porque contiene
todos las paginas y templates del proyecto. A diferencia de Angular 1 los templates deben estar incluidos
y no se pueden cargan asincronamente. Esto por cuestiones de rendimiento. Asi que dentro del archivo hay
muchos templates. 
Primero, en head nos encontramos el Content-Security-Policy el cual tiene filtros html para las imagenes,
css, js, data, iframes, etc. Toda la info necesaria para configurarlo esta aqui(https://developer.mozilla.org/es/docs/Web/HTTP/CSP)
Tambien en el head se cargan todos los css. Antes de publicar es necesario minificarlo y combinarlos.
Esto se detallara en la seccion de PENDIENTES.
En el body encontramos los templates, cada template es una pagina a mostrar en el app, todas deben
tener el elemento <v-ons-page> para encapsular la pagina, tambien cada pagina tiene un <v-ons-toolbar>
como elemento toolbar de las paginas, se encarga de abrir el menu, mostrar titulos, etc. La
directiva <v-ios-back-button> tiene configurada el boton de atras para dispositivos IOS, en Android
no aparece para dar utilidad al boton de atras. Los templates utilizan la interpolacion {{ dato }}
para renderizar los datos dentro. 
Hay un div llamado id="app" el cual inicializa el app, este utiliza el componente principal de app
y tiene un <v-ons-splitter> como componente principal. Este elemento es el encargado del menu lateral
disponible en todo el app, ademas se encarga de la navegacion. Dentro esta el elemento <router-view>
para se utiliza para navegar internamente entre paginas.(https://router.vuejs.org/)

# ==> FUNCIONAMIENTO DEL PROYECTO
El proyecto funciona hospedado en un componente principal de vuejs en el archivo js/app.component.js,
al cargar(el evento mounted) incializa las notificaciones de Firebase, en el caso de IOS debe solicitar
permiso para ello. Cualquier error generado en esta parte se envia al una lista de errores en 
Firebase. En el mounted tambien se setea el evento para el boton de atras de Android.
La navegacion en el app es manejada por el router(js/config/routes.router.js), funciona con el modo
hash que agrega un # al final de la routa y se puede configurar para recibir parametros por 
la url o propiedas enviadas directamente al componente(https://router.vuejs.org/guide/essentials/passing-props.html)
El boton de atras checkea si estamos en la pantalla de inicio, si es asi, pregunta para salir del app,
sino, navega atras.

El home, mas leidas, ultimas, categorias, temas, buscador y autor funcionan mas o menos de la misma 
manera, en el evento mounted llaman a las noticias a traves del servicio, ya sea filtrado por los
parametros del router o por propiedades.

Existe una ruta de 404, que cuando no funciona algo cae ahi, no deberia salir nunca, pero sucede asi
cuando borran una noticia o algun id esta malo.

La ruta de especiales tiene la particularidad que carga un json del sitio(https://www.crhoy.com/site/dist/json/especiales.app.json)
Este es un archivo estatico que debemos modificar y publicar nosotros, ya que somos nosotros quienes
nos encargamos de los especiales y de realizarlos. Al no poder insertar los especiales directamente
en el app, debemos emularlos como podemos, asi que desarrolle un metodo en el que se establece un array
con la configuracion de la vista del especial. En este podemos establecer como minisecciones del especial
tales como: imagen(para la portada del especial o una imagen con informacion), seccion de videos
por id(para establecer un carousel de videos pre-establecidos), una seccion de embeds en la que insertamos
en html el embed que queremos, una seccion de noticias por etiqueta y una para inyectar html(pero no scripts)

El componente de noticias es el mas complicado, lo primero es que recibe los parametros por el router,
la noticia se puede detectar por su url o por su id, con cualquiera de estos parametros se envia al servidor
para obtener la noticia(https://www.crhoy.com/site/generators/no-cache/new_single_app.php).
Al recibir la noticia se renderiza en pantalla en un componente(mas bien como una directiva) especial(contentComponent),
ya que esta no tiene template, sino una funcion para renderizar, la funcion render del componente
lo que hace es establecer un componente virtual con sus propios datos, metodos y eventos.

El componente de contenido depende mucho de lo que hicimos en el server. Eso lo explicamos ahora:
En la ruta /site/generators/no-cache/new_single_app.php, lo que hacemos basicamente es 
sobreescribir todos los shortcodes, tambien se filtran las categorias, tags, comentarios y scripts.
Hay que reorganizar la prioridad de los filtros de contenido, basicamente hay que eliminar y reasignar
el filtro de wpautop(https://codex.wordpress.org/Function_Reference/wpautop), wpex_fix_shortcodes
(https://github.com/jasoncomes/WordressShortcodes/blob/master/src/Shortcode.php) y do_shortcode
(https://developer.wordpress.org/reference/functions/do_shortcode/).
Para rendirizar la nota siempre debe ir todo el contenido dentro de un div o un elemento raiz, en 
nuestro caso es: <div id='nota-contenido' class='nota-contenido'>
En el archivo shortCodeToOnsenContent lo que hacemos es procesar los shortcodes para que devuelvan
contenido de onsen que nuestra app pueda interpretear. Existe una funcion llamada
overwrite_shortcodes_for_app que se encarga de remover y agregar los nuevos metodos para los shortcodes
ya registrados. Si se agrega un nuevo shortcode lo mejor es ingresarlo aqui pronto para que el app
lo pueda registrar. En el app no habria que hacer nada lo que facilita su mantenimiento.

# ==> COMO PUBLICAR EL APP
Para publicar el app, se puede utilizar el servicio de Monaca(Deploy service), el servicio de Firebase
deberia funcionar adecuadamente ya que existe un proyecto de Firebase para el app(CRHoy Lite). La otra
opcion es utilizar Monaca tambien, para generar una "build for release", la cual en el caso de android
requiere un keystore y un alias, esto se hace en la cuenta de crhoy para subir apps. En IOS basicamente
es lo mismo, hay que utilizar un certificado de produccion y un provisional profile para lo mismo tambien.

# ==> PENDIENTES
1- Hacer que en IOS reciba las notificaciones, en teoria el server esta listo para enviarlas,
pero no se porque no llegan, hay que revisar certificados, provisional profiles, ids y setear
los permisos bien. El server para enviar notificaciones esta listo, aunque falta publicarlo y probar 
que todo vaya bien.
2- Falta el diseño que se vaya a aplicar ya que nunca me lo pasaron.
3 - Falta probar el app en varios telefonos y tablets. Recomiendo el servicio de pruebas de firebase para esto.
4- Hay que conectar las notificaciones del sitio con las del app, en teoria firebase pueden enviar las notificaciones
a IOS, Android y web a la vez, pero hay que hacer que el sitio pueda recibirlas y manejarlas. Tanto en primer
plano como en el backgroud.