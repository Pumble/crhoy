/* https://github.com/arnesson/cordova-plugin-firebase
*/
const _notificacionesComponent = {
      name: "notificacionesComponent",
      template: "#notificaciones",
      data: function () {
            return {
                  notificationsList: [{
                        name: "Últimas noticias",
                        list: "noticias_importantes",
                        activate: false
                  }, {
                        name: "Nacionales",
                        list: "nacionales",
                        activate: false
                  }, {
                        name: "Deportes",
                        list: "deportes",
                        activate: false
                  }, {
                        name: "Entretenimiento",
                        list: "entretenimiento",
                        activate: false
                  }, {
                        name: "Mundo",
                        list: "mundo",
                        activate: false
                  } , {
                        name: "Pruebas",
                        list: "pruebas",
                        activate: false
                  }]
            };
      },
      mounted: function () {
            var auxNotificationsConfigList = JSON.parse(window.localStorage.getItem("notificationsConfigList"));
            if (auxNotificationsConfigList !== null && auxNotificationsConfigList !== undefined && auxNotificationsConfigList !== "") {
                  this.notificationsList = auxNotificationsConfigList;
            }
      },
      methods: {
            toogleNotificaction: function (ev, item) {
                  if (ev.value === true) {
                        window.FirebasePlugin.subscribe(item.list);
                  } else {
                        window.FirebasePlugin.unsubscribe(item.list);
                  }
                  this.saveNotificationsConfigList();
            },
            saveNotificationsConfigList: function () {
                  var self = this;
                  setTimeout(function () {
                        window.localStorage.setItem("notificationsConfigList", JSON.stringify(self.notificationsList));
                  }, 1000);
            }
      }
};